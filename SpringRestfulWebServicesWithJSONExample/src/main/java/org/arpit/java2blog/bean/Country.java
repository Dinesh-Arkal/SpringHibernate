 package org.arpit.java2blog.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Country")
public class Country{
	
	@Id
	@Column(name="Country_ID")
	int id;
	
	@Column(name="Country_name")
	String countryName;	
	
	public Country(int i, String countryName) {
		super();
		this.id = i;
		this.countryName = countryName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}	
	
}