package org.arpit.java2blog.hibernate.dao;

import java.util.List;
import java.util.logging.Logger;

import org.arpit.java2blog.bean.Country;
import org.arpit.java2blog.controller.CountryController;
import org.arpit.java2blog.hibernate.utility.HibernateUtility;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CountryDaoImpl implements CountryDao {

	static Logger log = Logger.getLogger(CountryController.class.getName());

	
	
	@Override
	public void addCountry(Country c) {
		
		SessionFactory factory=HibernateUtility.getInstance();
	     Session session =factory.openSession();
	     Transaction tx =session.beginTransaction();
	     session.save(c);
	     tx.commit();
	     session.close();
	 
	
	}

	@Override
	public void updateCountry(Country c) {
		// TODO Auto-generated method stub
		SessionFactory factory=HibernateUtility.getInstance();
	     Session session =factory.openSession();
	     Transaction tx =session.beginTransaction();
	     session.load(Country.class,c.getId());
	     c.setCountryName("NewZeland");
	     tx.commit();
	     session.close();
	}

	@Override
	public List<Country> listCountries() {
		SessionFactory factory=HibernateUtility.getInstance();
	     Session session =factory.openSession();
	     List<Country> countryList = session.createQuery("from Country c").list();
			session.close();
			return countryList;
	     
		
	
	}

	@Override
	public Country getCountryById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCountry(int id) {
		// TODO Auto-generated method stub

	}

}
