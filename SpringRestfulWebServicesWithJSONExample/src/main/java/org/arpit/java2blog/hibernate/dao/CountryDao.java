package org.arpit.java2blog.hibernate.dao;

import java.util.List;

import org.arpit.java2blog.bean.Country;

public interface CountryDao {

	
	public void addCountry(Country c);
    public void updateCountry(Country c);
    public List<Country> listCountries();
    public Country getCountryById(int id);
    public void removeCountry(int id);
	
	
	
}
