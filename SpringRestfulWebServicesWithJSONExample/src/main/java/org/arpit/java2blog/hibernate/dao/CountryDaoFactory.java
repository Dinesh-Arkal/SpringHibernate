package org.arpit.java2blog.hibernate.dao;

import java.util.logging.Logger;

public class CountryDaoFactory {
	
	static Logger log = java.util.logging.Logger.getLogger(CountryDaoFactory.class.getName());
	
	
	public static CountryDao getFactory(){
		
		log.info("entered into getFactory method");
		
		return new CountryDaoImpl();
	}

}
