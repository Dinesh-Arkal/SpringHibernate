package org.arpit.java2blog.hibernate.utility;

import java.util.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtility {
	
	static Logger log = Logger.getLogger(HibernateUtility.class.getName());
	
	private static SessionFactory factory=null;
	
	private HibernateUtility (){}
	
	@SuppressWarnings("deprecation")
	public static SessionFactory  getInstance(){
		
		log.info("Creating session factory object from utility ");
		try{
		if (factory==null){
			log.info("BuildSessionFactory is a deprecated method ");
			factory=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		
		}return factory;
	}catch(Exception e){
		log.info("Exception in session factory ");
	}return factory;
		
	}

}
