package org.arpit.java2blog.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.arpit.java2blog.bean.Country;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {

	static Logger log = Logger.getLogger(CountryController.class.getName());

	@RequestMapping(value = "/countries", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Country> getCountries() {
		log.info("entered into getCountries method");
		log.info("fetching response for all countries name");
		List<Country> listOfCountries = new ArrayList<Country>();
		listOfCountries = createCountryList();
		log.info("returning all countries name");
		return listOfCountries;
	}

	@RequestMapping(value = "/country/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Country getCountryById(@PathVariable int id) {
		List<Country> listOfCountries = new ArrayList<Country>();
		listOfCountries = createCountryList();

		for (Country country : listOfCountries) {
			if (country.getId() == id)
				return country;
		}

		return null;
	}

	// Utiliy method to create country list.
	public List<Country> createCountryList() {
		Country indiaCountry = new Country(1, "India");
		Country chinaCountry = new Country(4, "China");
		Country nepalCountry = new Country(3, "Nepal");
		Country bhutanCountry = new Country(2, "Bhutan");
		Country singaporeCountry = new Country(5, "singapore");
		Country srilankaCountry = new Country(6, "srilanka");

		List<Country> listOfCountries = new ArrayList<Country>();
		listOfCountries.add(indiaCountry);
		listOfCountries.add(chinaCountry);
		listOfCountries.add(nepalCountry);
		listOfCountries.add(bhutanCountry);
		listOfCountries.add(singaporeCountry);
		listOfCountries.add(srilankaCountry);
		return listOfCountries;
	}
}
